# Android Facebook SDK Install
## 1. 環境設置
* 安裝 [Python](https://www.python.org/downloads/) **只能使用 2.7.x 版本** **(安裝時勾選 Add Python to PATH 或自行設定 PATH)** 
* 安裝 SDKBox
    1. 透過 SDKBoxHelper 安裝 [Windows](https://github.com/hugohuang1111/sdkboxhelper/releases/download/v0.0.5/sdkboxhelper.exe)
    2. 使用 Python 指令安裝
    ```shell
    python -c """import urllib; s = urllib.urlopen('https://raw.githubusercontent.com/sdkbox-doc/en/master/install/install.py').read(); exec(s)"""
    ```
* 安裝 [Cocos Creator SDKBox GUI](http://sdkbox.anysdk.com/gui/creator/sdkbox-1.4.1.zip) 放置路徑 ${CocosCreator Project}/packages
## 2. 安裝 Facebook SDK
* 構建專案 > 安裝SDK > 撰寫代碼 > 重新構建
* 安裝方式 : 選取擴展 > SDKBox > Import Facebook SDK  **( 安裝前須先構建專案，安裝後會自動新增模組定義檔，有些沒有 )**
## 3. Android Studio Facebook SDK 設定 **( 請先申請 Facebook APP ID )**
1. your_app > Gradle Scripts > build.gradle（Project），確認 buildscript { repositories {}} 中列出了以下存放庫：
```java
buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }
}
```
2. 在專案中開啟 your_app > Gradle Scripts > build.gradle（Module：App_Name），然後將下列實作陳述式新增至 dependencies{} 區塊以使用最新版的 Facebook 登入 SDK：
```java
dependencies {
    implementation 'com.facebook.android:facebook-android-sdk:[4,5)'
}
```
3. 將 Facebook SDK 匯入你的應用程式 ( AppActivity )，並修改繼承：
```java
public class AppActivity extends com.sdkbox.plugin.SDKBoxActivity {

}
```
4. 開啟 /app/res/values/strings.xml 檔案新增以下項目：
```xml
<string name="facebook_app_id">App Id</string>
<string name="fb_login_protocol_scheme">fb+ App Id</string>
```
## 4.金鑰設置 ( 使用Keytool )
1. 安裝 [JRE](https://www.java.com/zh-TW/download/) 和 [OpenSSL 函式庫](https://code.google.com/archive/p/openssl-for-windows/downloads)
2. 產生 Keystore
```shell
keytool -genkey -v -keystore test.keystore -alias test -keyalg RSA -validity 10000 
```
3. 產生開發密鑰雜湊
    * Mac
    ```shell
    keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
    ```
    * Windows
    ```shell
    keytool -exportcert -alias androiddebugkey -keystore "C:\Users\USERNAME\.android\debug.keystore" | "PATH_TO_OPENSSL_LIBRARY\bin\openssl" sha1 -binary | "PATH_TO_OPENSSL_LIBRARY\bin\openssl" base64
    ```
4. 產生發行密鑰雜湊
* Android 應用程式必須先以發行密鑰雜湊進行數位簽署，才能上傳至商店。若要產生發行密鑰雜湊，請在 Mac 或 Windows 上執行下列指令，並替換成您的發行密鑰別名和 KeyStore 路徑：
```shell
keytool -exportcert -alias YOUR_RELEASE_KEY_ALIAS -keystore YOUR_RELEASE_KEY_PATH | openssl sha1 -binary | openssl base64
```
## 5. 發布
* 使用含金鑰方式發布 Facebook api 才能使用
