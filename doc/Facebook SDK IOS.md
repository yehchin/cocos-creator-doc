# IOS Facebook SDK Install
## 1. 環境設置
* 安裝 [Python](https://www.python.org/downloads/) **只能使用 2.7.x 版本** **(安裝時勾選 Add Python to PATH 或自行設定 PATH)** 
* 安裝 SDKBox
    1. 透過 SDKBoxHelper 安裝 [Mac](https://github.com/hugohuang1111/sdkboxhelper/releases/download/v0.0.5/sdkboxhelper)
    2. 使用 Python 指令安裝
    ```shell
    python -c """import urllib; s = urllib.urlopen('https://raw.githubusercontent.com/sdkbox-doc/en/master/install/install.py').read(); exec(s)"""
    ```
* 安裝 [Cocos Creator SDKBox GUI](http://sdkbox.anysdk.com/gui/creator/sdkbox-1.4.1.zip) 放置路徑 ${CocosCreator Project}/packages
## 2. 安裝 Facebook SDK
* 構建專案 > 安裝SDK > 撰寫代碼 > 重新構建
* 安裝方式 : 選取擴展 > SDKBox > Import Facebook SDK  **( 安裝前須先構建專案，安裝後會自動新增模組定義檔，有些沒有 )**
## 3. Xcode Facebook SDK 設定 **( 請先申請 Facebook APP ID )**
1. 在 Info.plist 上點擊右鍵，然後選擇以原始碼形式開啟。複製以下 XML 程式碼片段並貼至檔案主體（<dict>...</dict>）
```xml
<key>CFBundleURLTypes</key>
<array>
  <dict>
  <key>CFBundleURLSchemes</key>
  <array>
    <string>fb+AppId</string>
  </array>
  </dict>
</array>
<key>FacebookAppID</key>
<string>AppId</string>
<key>FacebookDisplayName</key>
<string>BFP_test</string>

<key>LSApplicationQueriesSchemes</key>
<array>
  <string>fbapi</string>
  <string>fb-messenger-api</string>
  <string>fbauth2</string>
  <string>fbshareextension</string>
</array>
```
2. 追蹤應用程式安裝和應用程式開啟
* 將以下內容新增到你的應用程式委派中：
```swift
#import <FBSDKCoreKit/FBSDKCoreKit.h>

- (void)applicationDidBecomeActive:(UIApplication *)application {
  [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
  return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                         openURL:url
                                               sourceApplication:sourceApplication
                                                      annotation:annotation];
}
```
3. 新增一個 Swift file 選擇 Create Bridging Header 且不要刪除 Swift file
4. Build Settings > Search Paths > Always Search User Path (Deprecated) > yes
